package com.cafe.data.model;

public class FoursquareLocation {

    // The address of the location.
    String address;

    // The latitude of the location.
    double lat;

    // The longitude of the location.
    double lng;

    // The distance of the location, calculated from the specified location.
    int distance;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
}