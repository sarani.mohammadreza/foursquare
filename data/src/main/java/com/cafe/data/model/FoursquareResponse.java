package com.cafe.data.model;

import java.util.ArrayList;
import java.util.List;

public class FoursquareResponse {

    // A group object within the response.
    FoursquareGroup group;
//    List<FoursquareVenue> venues = new ArrayList<>();
//    List<FoursquareCategories> categories = new ArrayList<>();


    List<FoursquareVenue> venues = new ArrayList<>();

    public List<FoursquareVenue> getVenues() {
        return venues;
    }

    public void setVenues(List<FoursquareVenue> venues) {
        this.venues = venues;
    }

    public FoursquareGroup getGroup() {
        return group;
    }

    public void setGroup(FoursquareGroup group) {
        this.group = group;
    }


}