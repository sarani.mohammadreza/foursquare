package com.cafe.data.model;

public class FoursquareResults {

    // A venue object within the results.
    FoursquareVenue venue;

    public FoursquareVenue getVenue() {
        return venue;
    }

    public void setVenue(FoursquareVenue venue) {
        this.venue = venue;
    }
}
