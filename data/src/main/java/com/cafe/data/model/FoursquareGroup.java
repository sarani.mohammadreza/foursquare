package com.cafe.data.model;

import java.util.ArrayList;
import java.util.List;

public class FoursquareGroup {

    // A results list within the group.
    List<FoursquareResults> results = new ArrayList<FoursquareResults>();

    public List<FoursquareResults> getResults() {
        return results;
    }

    public void setResults(List<FoursquareResults> results) {
        this.results = results;
    }
}
