package com.cafe.data.factory;

import com.cafe.data.remote.Api;

import javax.inject.Inject;

public class DataStoreFactory
{
    private Api api;

    @Inject
    public DataStoreFactory(Api api) {
        this.api = api;
    }


    public DataStore dataStore()
    {
        return new DataStoreRemoteImpl(api);
    }
}
