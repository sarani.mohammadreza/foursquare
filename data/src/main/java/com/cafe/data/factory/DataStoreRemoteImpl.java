package com.cafe.data.factory;

import com.cafe.data.model.FoursquareJSON;
import com.cafe.data.remote.Api;

import io.reactivex.rxjava3.core.Single;

public class DataStoreRemoteImpl implements DataStore {

    private Api api;

    public DataStoreRemoteImpl(Api api) {
        this.api = api;
    }

    @Override
    public Single<FoursquareJSON> getVenue(String clientID, String clientSecret, String ll, double llAcc, int limit) {
        return api.goToPlace( clientID , clientSecret , ll , llAcc , limit );
    }

    @Override
    public Single<FoursquareJSON> getVenueWithName(String clientID, String clientSecret, String ll, double llAcc, String placeName, int limit) {
        return api.goToPlaceSearch(clientID , clientSecret , ll , llAcc , placeName , limit);
    }
}
