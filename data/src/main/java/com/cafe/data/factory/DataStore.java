package com.cafe.data.factory;


import com.cafe.data.model.FoursquareJSON;

import io.reactivex.rxjava3.core.Single;

public interface DataStore
{
    Single<FoursquareJSON> getVenue        (String clientID , String clientSecret , String ll , double llAcc , int limit );
    Single<FoursquareJSON> getVenueWithName(String clientID , String clientSecret , String ll , double llAcc , String placeName ,int limit);

}
