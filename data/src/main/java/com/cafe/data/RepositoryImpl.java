package com.cafe.data;

import com.cafe.data.factory.DataStoreFactory;
import com.cafe.data.mapper.VenueModelToMainDataMapper;
import com.cafe.data.mapper.VenueModelToMainDataWithNameMapper;
import com.cafe.domain.Repository;
import com.cafe.domain.model.DataMainDomain;

import java.util.List;

import io.reactivex.rxjava3.core.Single;


import javax.inject.Inject;


public class RepositoryImpl  implements Repository {

    private final DataStoreFactory dataStore;

    @Inject
    public RepositoryImpl(DataStoreFactory dataStore) {
        this.dataStore = dataStore;
    }

    @Override
    public Single<List<DataMainDomain>> getVenue(String clientID , String clientSecret , String ll , double llAcc , int limit )
    {
        return dataStore.dataStore().getVenue(clientID , clientSecret , ll , llAcc , limit).map(foursquareJSON -> new VenueModelToMainDataMapper().apply(foursquareJSON.getResponse().getVenues()));
    }

    @Override
    public Single<List<DataMainDomain>> getVenueWithName(String clientID , String clientSecret , String ll , double llAcc , String placeName ,int limit)
    {
        return dataStore.dataStore().getVenueWithName(clientID , clientSecret , ll , llAcc , placeName , limit).map(foursquareJSON -> new VenueModelToMainDataWithNameMapper().apply(foursquareJSON.getResponse().getGroup().getResults()));
    }
}
