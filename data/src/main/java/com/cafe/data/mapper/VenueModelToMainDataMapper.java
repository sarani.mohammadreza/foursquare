package com.cafe.data.mapper;

import android.util.Log;

import com.cafe.data.model.FoursquareVenue;
import com.cafe.domain.model.DataMainDomain;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.functions.Function;

public class VenueModelToMainDataMapper implements Function<List<FoursquareVenue>, List<DataMainDomain>>
{
    @Override
    public List<DataMainDomain> apply(List<FoursquareVenue> list)
    {
        return transfer(list);
    }

    private DataMainDomain transfer(FoursquareVenue data)
    {
        if (data==null)
            throw new IllegalArgumentException("List Is Empty");

        DataMainDomain data1 = new DataMainDomain();

        data1.setId        (data.getId());
        data1.setLatitude  (data.getLocation().getLat());
        data1.setLongitude (data.getLocation().getLng());
        data1.setName      (data.getName());

  //      data1.setPhoto     (data.getCategories().getIcon().getSuffix() + data.getCategories().getIcon().getPrefix());
        data1.setRate      (data.getRating());
        data1.setDistance  (data.getLocation().getDistance());

        data1.setAddress(data.getLocation().getAddress());

        return data1;
    }


    private List<DataMainDomain> transfer(List<FoursquareVenue> list)
    {
        final List<DataMainDomain> userList = new ArrayList<>();

        for (FoursquareVenue data : list) {
            final DataMainDomain user = transfer(data);
            userList.add(user);
        }

        return userList;
    }
}
