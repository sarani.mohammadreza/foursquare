package com.cafe.data.mapper;

import com.cafe.data.model.FoursquareResults;
import com.cafe.domain.model.DataMainDomain;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.functions.Function;

public class VenueModelToMainDataWithNameMapper implements Function<List<FoursquareResults>, List<DataMainDomain>>
{
    @Override
    public List<DataMainDomain> apply(List<FoursquareResults> list)
    {
        return transfer(list);
    }

    private DataMainDomain transfer(FoursquareResults data)
    {
        if (data==null)
            throw new IllegalArgumentException("List Is Empty");

        DataMainDomain data1 = new DataMainDomain();

        data1.setId        (data.getVenue().getId());
        data1.setLatitude  (data.getVenue().getLocation().getLat());
        data1.setLongitude (data.getVenue().getLocation().getLng());
        data1.setName      (data.getVenue().getName());
      //  data1.setPhoto     (data.getVenue().get().getIcon().getSuffix() + data.getVenue().getCategories().getIcon().getPrefix());
        data1.setRate      (data.getVenue().getRating());
        data1.setDistance  (data.getVenue().getDistance());

        return data1;
    }


    private List<DataMainDomain> transfer(List<FoursquareResults> list)
    {
        final List<DataMainDomain> userList = new ArrayList<>();

        for (FoursquareResults data : list) {
            final DataMainDomain user = transfer(data);
            userList.add(user);
        }

        return userList;
    }
}
