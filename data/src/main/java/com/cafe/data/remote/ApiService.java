package com.cafe.data.remote;

import android.content.Context;

import com.readystatesoftware.chuck.ChuckInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


import javax.inject.Inject;

public class ApiService
{
    private final String Url ;

    private  Context context;
    @Inject
    public ApiService(Context context)
    {
        this.context = context;
        Url = "https://api.foursquare.com/v2/";
    }

/*    static ApiService create()
    {
        return new ApiService();
    }*/


    private Retrofit getRetrofit()
    {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60 ,TimeUnit.SECONDS)
                .connectTimeout(60,TimeUnit.SECONDS)
        ;

        // execute Asynchronously
        // enqueue Synchronously
        return new Retrofit
                .Builder()
                .baseUrl(Url)
                .client(builder.build())
                .client(new OkHttpClient.Builder().addInterceptor(new ChuckInterceptor(context)).build())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public <T> T  create(final Class<T> apiService)
    {return getRetrofit().create(apiService);}
}
