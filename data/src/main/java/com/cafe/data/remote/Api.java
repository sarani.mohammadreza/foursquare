package com.cafe.data.remote;

import com.cafe.data.model.FoursquareJSON;

import io.reactivex.rxjava3.core.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api
{
    // A request to snap the current user to a place via the Foursquare API.
    @GET("venues/search?v=20161101")
    Single<FoursquareJSON> goToPlace(@Query("client_id") String clientID,
                                       @Query("client_secret") String clientSecret,
                                       @Query("ll") String ll,
                                       @Query("llAcc") double llAcc,
                                       @Query("limit") int limit);

    // A request to search for nearby Place recommendations via the Foursquare API.
    @GET("search/recommendations?v=20161101")
    Single<FoursquareJSON> goToPlaceSearch(@Query("client_id") String clientID,
                                      @Query("client_secret") String clientSecret,
                                      @Query("ll") String ll,
                                      @Query("llAcc") double llAcc,
                                      @Query("intent") String placeName,
                                      @Query("limit") int limit);
}
