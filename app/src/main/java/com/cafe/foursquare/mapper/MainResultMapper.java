package com.cafe.foursquare.mapper;

import com.cafe.domain.model.DataMainDomain;
import com.cafe.foursquare.model.DataMain;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.functions.Function;


public class MainResultMapper implements Function<List<DataMainDomain>, List<DataMain>>
{
        @Override
        public List<DataMain> apply(List<DataMainDomain> list)
        {
            return transfer(list);
        }

        private DataMain transfer(DataMainDomain data)
        {
            if (data==null)
                throw new IllegalArgumentException("List Is Empty");

            DataMain data1 = new DataMain();

            data1.setId        (data.getId());
            data1.setLatitude  (data.getLatitude());
            data1.setLongitude (data.getLongitude());
            data1.setName      (data.getName());
            data1.setPhoto     (data.getPhoto());
            data1.setRate      (data.getRate());
            data1.setDistance  (data.getDistance());
            data1.setAddress   (data.getAddress());

            return data1;
        }


        private List<DataMain> transfer(List<DataMainDomain> list)
        {
            final List<DataMain> userList = new ArrayList<>();

            for (DataMainDomain data : list) {
                final DataMain user = transfer(data);
                userList.add(user);
            }

            return userList;
        }
}