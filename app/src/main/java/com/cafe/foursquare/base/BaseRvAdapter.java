package com.cafe.foursquare.base;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRvAdapter<T, E extends RvViewHolder<T>> extends RecyclerView.Adapter<E> {

    protected List<T> items = new ArrayList<>();

    public BaseRvAdapter() {

    }


    public BaseRvAdapter(List<T> items)
    {
        this.items = items;
    }

    public void setItems(List<T> items)
    {
        this.items = items;
    }

    public void clearList()
    {
        if (this.items.size() > 0)
        {
            this.items.clear();
             notifyDataSetChanged();
        }
    }
    @Override
    public int getItemCount()
    {
        return items != null ? items.size() : 0;
    }

    public void addView(List<T> items)
    {
        this.items.addAll(items);
        //  notifyDataSetChanged();
          notifyItemRangeInserted(this.items.size() - items.size(), items.size());
    }

    @Override
    public void onBindViewHolder(@NonNull E holder, int position) {
        holder.bind(items.get(position));
    }
}
