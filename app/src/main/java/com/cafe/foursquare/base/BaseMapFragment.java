package com.cafe.foursquare.base;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.cafe.foursquare.R;

import java.net.UnknownHostException;
import java.util.Objects;

public abstract class BaseMapFragment extends FragmentActivity implements BaseItem {


    protected View view = null;
    @LayoutRes
    public abstract int getLayout();

    public abstract void PageInit(@Nullable Bundle savedInstanceState);


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());

        PageInit(savedInstanceState);
    }



    @Override
    public void ShowError(String TAG, Throwable throwable) {

        Log.e(TAG, throwable.toString(), throwable);
        if (throwable instanceof UnknownHostException)
            Toast.makeText((Objects.requireNonNull(getParent())), getString(R.string.error_no_internet) , Toast.LENGTH_SHORT).show();
        else
            Toast.makeText((Objects.requireNonNull(getParent())), getString(R.string.error) , Toast.LENGTH_SHORT).show();
    }


    @Override
    public void showProgress()
    {
    }

    @Override
    public void hideProgress()
    {
    }
}
