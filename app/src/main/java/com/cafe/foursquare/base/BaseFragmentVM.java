package com.cafe.foursquare.base;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.cafe.foursquare.R;

import java.net.UnknownHostException;
import java.util.Objects;

public abstract class BaseFragmentVM<VM extends BaseViewModel> extends Fragment implements BaseItem {


    protected View view = null;

    protected VM viewModel;

/*    @LayoutRes
    public abstract int getLayout();*/

    public abstract void PageInit(@Nullable Bundle savedInstanceState);

    protected abstract VM createViewModel();


/*    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {

        if (view == null) {
            view = inflater.inflate(getLayout(), container, false);
        }
        return view;
    }*/


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = createViewModel();

        PageInit(savedInstanceState);
    }



    @Override
    public void ShowError(String TAG, Throwable throwable) {

        Log.e(TAG, throwable.toString(), throwable);
        if (throwable instanceof UnknownHostException)
            Toast.makeText((Objects.requireNonNull(getActivity())), getString(R.string.error_no_internet) , Toast.LENGTH_SHORT).show();
        else
            Toast.makeText((Objects.requireNonNull(getActivity())), getString(R.string.error) , Toast.LENGTH_SHORT).show();
    }


    @Override
    public void showProgress()
    {
    }

    @Override
    public void hideProgress()
    {
    }
}
