package com.cafe.foursquare.base;

public interface BaseItem
{
      void showProgress();

      void hideProgress();

      void ShowError(String TAG, Throwable throwable);
}

