package com.cafe.foursquare.base;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.ButterKnife;

public abstract class RvViewHolder<T> extends RecyclerView.ViewHolder {
    public RvViewHolder(@NonNull View itemView)
    {
        super(itemView);
        ButterKnife.bind(this , itemView);
    }

    public abstract void bind(T item);
}
