package com.cafe.foursquare.base;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.cafe.foursquare.R;

import java.net.UnknownHostException;

public abstract class BaseActivity extends AppCompatActivity  implements BaseItem{


    @LayoutRes
    public abstract int getLayout();


    public abstract void pageInit(@Nullable Bundle savedInstanceState);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayout());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            getWindow().getDecorView().setSystemUiVisibility(0);
        }

        pageInit(savedInstanceState);

    }


    @Override
    public void ShowError(String TAG, Throwable throwable) {

        Log.e(TAG, throwable.toString(), throwable);

        if (throwable instanceof UnknownHostException) {
            Toast.makeText(this, getString(R.string.error_no_internet) , Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error) , Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void showProgress()
    {
    }

    @Override
    public void hideProgress()
    {
    }
}
