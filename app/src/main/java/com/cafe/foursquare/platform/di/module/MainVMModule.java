package com.cafe.foursquare.platform.di.module;

import android.content.Context;

import com.cafe.data.RepositoryImpl;
import com.cafe.data.factory.DataStoreFactory;
import com.cafe.data.remote.Api;
import com.cafe.data.remote.ApiService;
import com.cafe.domain.Repository;
import com.cafe.domain.intractor.factory.UseCaseFactory;
import com.cafe.foursquare.platform.di.scope.MainVM;
import dagger.Module;
import dagger.Provides;


@Module
public class MainVMModule
{
    @Provides
    @MainVM
    public Repository getRepository(DataStoreFactory dataStore)
    {
        return new RepositoryImpl(dataStore);
    }

    @Provides
    @MainVM
    public UseCaseFactory getUseCaseFactory(Repository repository)
    {
        return new UseCaseFactory(repository);
    }
    @Provides
    @MainVM
    public DataStoreFactory getDataStore(Api api)
    {
        return new DataStoreFactory(api);
    }
    @Provides
    @MainVM
    public Api getApi(Context context)
    {
       return new ApiService(context).create(Api.class);
    }
}
