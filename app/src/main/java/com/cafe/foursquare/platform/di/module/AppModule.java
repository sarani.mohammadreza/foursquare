package com.cafe.foursquare.platform.di.module;

import android.content.Context;

import com.cafe.foursquare.app;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class AppModule
{
    private final app app;

    public AppModule(app app) {
        this.app = app;
    }
    @Singleton
    @Provides
    public Context getContext()
    {
        return app;
    }
}
