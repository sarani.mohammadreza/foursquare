package com.cafe.foursquare.platform.di.component;

import android.content.Context;

import com.cafe.foursquare.platform.di.module.AppModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {AppModule.class})
@Singleton
public interface AppComponent
{
    Context context();
}

