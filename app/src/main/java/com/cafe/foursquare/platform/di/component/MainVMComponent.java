package com.cafe.foursquare.platform.di.component;

import com.cafe.foursquare.platform.di.module.MainVMModule;
import com.cafe.foursquare.platform.di.scope.MainVM;
import com.cafe.foursquare.ui.fragment.main.MainFViewModel;

import dagger.Component;

@MainVM
@Component(dependencies = {AppComponent.class} ,   modules = {MainVMModule.class}  )

public interface MainVMComponent
{
    void inject(MainFViewModel VM);
}
