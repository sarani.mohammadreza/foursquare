package com.cafe.foursquare.ui.fragment.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.cafe.foursquare.R;
import com.cafe.foursquare.base.BaseRvAdapter;
import com.cafe.foursquare.base.RvViewHolder;
import com.cafe.foursquare.model.DataMain;
import com.cafe.foursquare.ui.fragment.map.MapFragment;

import butterknife.BindView;

public class MainAdapter extends BaseRvAdapter<DataMain , MainAdapter.MainViewHolder> {

    private final Context context;

    public MainAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MainViewHolder(LayoutInflater.from(context).inflate(R.layout.row_main , parent  , false));
    }

    @SuppressLint("NonConstantResourceId")
    class MainViewHolder extends RvViewHolder<DataMain> {

        @BindView(R.id.tvName)
          TextView tvName;
        @BindView(R.id.tvAddress)
          TextView tvAddress;
        @BindView(R.id.tvRating)
          TextView tvRating;
        @BindView(R.id.tvDistance)
          TextView tvDistance;

        DataMain dataMain;

        public MainViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(s->{

            // send data from main fragment to Map Fragment
            Intent i = new Intent(context , MapFragment.class);
            i.putExtra("name"     , dataMain.getName());
            i.putExtra("ID"       , dataMain.getId());
            i.putExtra("latitude" , dataMain.getLatitude());
            i.putExtra("name"     , dataMain.getLongitude());
            context.startActivity(i);
            });
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void bind(DataMain item) {
            dataMain = item;

            double ratingRaw =item.getRate();
            if (ratingRaw == 0) {
                tvRating.setVisibility(View.INVISIBLE);
            } else if (ratingRaw >= 8.0) {
                tvRating.setBackgroundColor(ContextCompat.getColor(context, R.color.FSQGuacamole));
            } else if (ratingRaw >= 7.0) {
                tvRating.setBackgroundColor(ContextCompat.getColor(context, R.color.FSQLime));
            } else if (ratingRaw >= 6.0) {
                tvRating.setBackgroundColor(ContextCompat.getColor(context, R.color.FSQBanana));
            } else if (ratingRaw >= 5.0) {
                tvRating.setBackgroundColor(ContextCompat.getColor(context, R.color.FSQOrange));
            } else if (ratingRaw >= 4.0) {
                tvRating.setBackgroundColor(ContextCompat.getColor(context, R.color.FSQMacCheese));
            }else if (ratingRaw >= 9.0) {
                tvRating.setBackgroundColor(ContextCompat.getColor(context, R.color.FSQKale));
            }
            else
                tvRating.setBackgroundColor(ContextCompat.getColor(context, R.color.FSQStrawberry));


            // Sets each view with the appropriate venue details
           tvName       .setText(item.getName());
           tvAddress    .setText(item.getAddress());
           tvRating     .setText(String.valueOf(ratingRaw));
           tvDistance   .setText(item.getDistance() + "m");
        }
    }

}
