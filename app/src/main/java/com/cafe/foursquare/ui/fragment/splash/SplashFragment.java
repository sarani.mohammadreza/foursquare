package com.cafe.foursquare.ui.fragment.splash;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;


import android.os.Handler;
import android.os.Looper;


import com.cafe.foursquare.R;
import com.cafe.foursquare.base.BaseFragment;
import com.cafe.foursquare.databinding.FragmentSplashBinding;
import com.cafe.foursquare.ui.activity.MainActivity;

import java.util.Objects;


@SuppressLint("ValidFragment")
public class SplashFragment extends BaseFragment {


    private MainActivity activity;


    public SplashFragment()
    {
    }

    @Override
    public int getLayout()
    {
        return R.layout.fragment_splash;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        activity = (MainActivity) getActivity();
    }

    @Override
    public void PageInit(@Nullable Bundle savedInstanceState)
    {


        FragmentSplashBinding binding = DataBindingUtil.setContentView(activity, getLayout());

        NavHostFragment navHostFragment = (NavHostFragment) activity.getSupportFragmentManager()
                .findFragmentById(R.id.navHostFragment);
        NavController navCo = Objects.requireNonNull(navHostFragment).getNavController();

        binding.tvSplash.setOnClickListener(s-> navCo.navigate(R.id.mainFragment));

        // Navigate To Main Fragment
        new Handler(Looper.getMainLooper()).postDelayed(() -> NavHostFragment.findNavController(SplashFragment.this).navigate(R.id.mainFragment), 1500);

    }

}