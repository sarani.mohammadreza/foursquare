package com.cafe.foursquare.ui.activity;

import androidx.annotation.Nullable;
import androidx.navigation.Navigation;

import android.os.Bundle;

import com.cafe.foursquare.R;
import com.cafe.foursquare.base.BaseActivity;

public class MainActivity extends BaseActivity {


    //private Fragment navHostFragment;


    @Override
    public int getLayout()
    {
        return R.layout.activity_main;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void pageInit(@Nullable Bundle savedInstanceState) {
        Navigation.findNavController(this , R.id.navHostFragment);
    }


}