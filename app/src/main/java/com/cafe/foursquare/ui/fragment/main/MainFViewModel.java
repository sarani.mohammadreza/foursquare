package com.cafe.foursquare.ui.fragment.main;

import android.app.Application;

import android.util.Log;

import androidx.annotation.NonNull;


import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.OnLifecycleEvent;

import com.cafe.data.RepositoryImpl;
import com.cafe.data.factory.DataStoreFactory;
import com.cafe.data.remote.Api;
import com.cafe.data.remote.ApiService;
import com.cafe.domain.Repository;
import com.cafe.domain.intractor.base.UseCase;
import com.cafe.domain.intractor.factory.UseCaseFactory;
import com.cafe.domain.model.DataMainDomain;
import com.cafe.domain.model.DataVenueInputDomain;
import com.cafe.domain.model.DataVenueWithNameInputDomain;
import com.cafe.foursquare.app;
import com.cafe.foursquare.base.BaseViewModel;
import com.cafe.foursquare.mapper.MainResultMapper;
import com.cafe.foursquare.model.DataMain;
import com.cafe.foursquare.platform.di.component.DaggerMainVMComponent;
import com.cafe.foursquare.platform.di.module.MainVMModule;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;


public class MainFViewModel extends BaseViewModel implements UseCase.CallBack<Single<List<DataMainDomain>>> , LifecycleObserver {

    @Inject
    UseCaseFactory caseFactory;

    private final CompositeDisposable disposable = new CompositeDisposable();

    public MutableLiveData<List<DataMain>> mainMutableLiveData = new MutableLiveData<>();


    public MainFViewModel(@NonNull Application application ) {
        super(application);

        // Handle Dependency Injection With Dagger
        DaggerMainVMComponent.builder()
                .appComponent(app.getAppComponent())
                .mainVMModule(new MainVMModule())
                .build().inject(this);

        initItems();
    }


    public void initItems()
    {
/*        Api api = new ApiService(app.getContext()).create(com.cafe.data.remote.Api.class);
        DataStoreFactory storeFactory = new DataStoreFactory(api);
        Repository repository = new RepositoryImpl(storeFactory);
        caseFactory = new UseCaseFactory(repository);*/

    }

    public void invokeUseCaseGetVenue(DataVenueInputDomain data)
    {
         // execute Use Case On Domain Module
        UseCase useCase = caseFactory.getVenueUseCase();

        useCase.execute(data , this);


    }


    public void invokeUseCaseGetVenueWithName(DataVenueWithNameInputDomain data)
    {
        UseCase useCase = caseFactory.GetVenueWithNameUseCase();

        useCase.execute(data , this);
    }

    @Override
    public void onSuccess(Single<List<DataMainDomain>> data) {

          data
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<DataMainDomain>>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull List<DataMainDomain> dataMainDomains)
                    {
                        mainMutableLiveData.postValue(new MainResultMapper().apply(dataMainDomains));
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e)
                    {
                    }
                });
    }

    @Override
    public void onError(Throwable throwable) {

    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    protected void onLifeCycleStop() {
        disposable.dispose();
    }
}