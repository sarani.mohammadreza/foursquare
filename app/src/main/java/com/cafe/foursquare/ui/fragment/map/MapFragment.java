package com.cafe.foursquare.ui.fragment.map;

import android.annotation.SuppressLint;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.core.app.NavUtils;
import androidx.core.content.ContextCompat;


import com.cafe.foursquare.R;

import com.cafe.foursquare.base.BaseMapFragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

@SuppressLint("ValidFragment")
public class MapFragment extends BaseMapFragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private String venueID;
    private String venueName;
    private double venueLatitude;
    private double venueLongitude;

    public MapFragment()
    {
    }

    @Override
    public int getLayout()
    {
        return R.layout.fragment_map;
    }

    @Override
    public void PageInit(@Nullable Bundle savedInstanceState)
    {
/*        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);*/

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        Bundle venue    = getIntent().getExtras();
        venueID         = venue.getString("ID");
        venueName       = venue.getString("name");
        venueLatitude   = venue.getDouble("latitude");
        venueLongitude  = venue.getDouble("longitude");
        setTitle(venueName);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng venue = new LatLng(venueLatitude, venueLongitude);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(venue, 14));

        Marker marker = googleMap.addMarker(new MarkerOptions()
                .position(venue)
                .title(venueName)
                .snippet("View on Foursquare"));
        marker.showInfoWindow();
        googleMap.setOnInfoWindowClickListener(this);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            // Shows the user's current location
            googleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        // Opens the Foursquare venue page
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://foursquare.com/v/" + venueID));
        startActivity(browserIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}