package com.cafe.foursquare.ui.fragment.main;

import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cafe.domain.model.DataVenueInputDomain;
import com.cafe.foursquare.R;
import com.cafe.foursquare.app;
import com.cafe.foursquare.base.BaseFragmentVM;
import com.cafe.foursquare.databinding.FragmentMainBinding;
import com.cafe.foursquare.ui.activity.MainActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.jetbrains.annotations.NotNull;

@SuppressLint("ValidFragment")
public class MainFragment extends BaseFragmentVM<MainFViewModel> {


    private static final int REQUEST_LOCATION = 1001;
    private FragmentMainBinding binding;

    private MainActivity activity;

    //GoogleApiClient googleApiClient;
    // The client object for connecting to the Google API
    private FusedLocationProviderClient fusedLocationProviderClient;


    private MainAdapter adapter;

    private int Limit = 0;

    private String userLL = "";

    private double userAccuracy = 0;



    public MainFragment()
    {
    }



    @Override
    protected MainFViewModel createViewModel() {
        return ViewModelProviders.of(this).get(MainFViewModel.class);
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        activity = (MainActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
         binding = FragmentMainBinding.inflate(inflater, container, false);
         return binding.getRoot();
    }

    @Override
    public void PageInit(@Nullable Bundle savedInstanceState)
    {
        initViews();

        getLocation();


        viewModel.mainMutableLiveData.observe(this, dataMains ->
                {
                    adapter.addView(dataMains);
                    binding.pbMain.setVisibility(View.GONE);
                }
        );

    }

    private void initViews() {
        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
            adapter.clearList();
            Limit = 0;
            getLocation();
        });


        // EndLess Recycler View
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);
        binding.rvMain.setLayoutManager(mLayoutManager);

        // Fetch New Data To RV
        binding.rvMain.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0)
                {
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount   = mLayoutManager.getItemCount();
                    int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount)
                        {
                            binding.pbMain.setVisibility(View.VISIBLE);
                            requestToNew();
                            }
                } }
        });


        adapter = new MainAdapter(activity);

        binding.rvMain.setAdapter(adapter);
    }


    public void getLocation()
    {
        // Swipe Refresh Layout
        if (binding.swipeRefreshLayout.isRefreshing())
            binding.swipeRefreshLayout.setRefreshing(false);

        binding.pbMain.setVisibility(View.VISIBLE);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(app.getContext());
        if (ActivityCompat.checkSelfPermission(app.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(app.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationProviderClient.requestLocationUpdates(new LocationRequest(), new LocationCallback(), Looper.myLooper());

        fusedLocationProviderClient.getLastLocation().addOnCompleteListener(activity, task -> {
            if (task.getResult() == null)
                requestNewLocationData();
            else {
                // Get User Lat And Lag And User Accuracy for Send Server
                userLL       = task.getResult().getLatitude() + "," +  task.getResult().getLongitude();
                userAccuracy = task.getResult().getAccuracy();
                // Request To Net
                requestToNew();
            }
        });


    }


    private void requestNewLocationData() {
        LocationRequest request = new LocationRequest();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setInterval(0);
        request.setFastestInterval(0);
        request.setNumUpdates(1);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity);
        if (ActivityCompat.checkSelfPermission(app.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(app.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationProviderClient.requestLocationUpdates(new LocationRequest(), new LocationCallback(), Looper.myLooper());

    }

    private void requestToNew()
    {
        // Because Max limit Value From Api is 50
        if (Limit > 50)
        {
            binding.pbMain.setVisibility(View.GONE);
            Toast.makeText(activity, "Max Item is Visible", Toast.LENGTH_SHORT).show();
            return;
        }
        // Get 5 Other Object
        Limit +=12;

        DataVenueInputDomain data = new DataVenueInputDomain();
        data.setClientID(app.getContext().getResources().getString(R.string.foursquare_client_id));
        data.setClientSecret(app.getContext().getResources().getString(R.string.foursquare_client_secret));
        data.setLimit(Limit);
        data.setLlAcc(userAccuracy);
        data.setLl(userLL);

        viewModel.invokeUseCaseGetVenue(data);

    }


    @Override
    public void onStart() {
        super.onStart();
        // Run Time Check Permission
        if (ActivityCompat.checkSelfPermission(activity.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(activity.getApplicationContext(),
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        } else {
            Log.d("DB", "PERMISSION GRANTED");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // When Permission  Granted Then Get Location
        if (requestCode == REQUEST_LOCATION) {
            if (permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION)
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLocation();
            }
        }
    }

}