package com.cafe.foursquare;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.cafe.foursquare.platform.di.component.AppComponent;
import com.cafe.foursquare.platform.di.component.DaggerAppComponent;
import com.cafe.foursquare.platform.di.module.AppModule;

import java.lang.ref.WeakReference;

public class app extends Application implements LifecycleObserver {
    private static WeakReference<Context> contextWeakReference;

    private static AppComponent appComponent;
    @Override
    public void onCreate() {
        super.onCreate();

        contextWeakReference = new WeakReference<>(getApplicationContext());

        // Set LifeCycle Observer To hole Application
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);


        // App Component
        if (appComponent == null)
            appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

    }

    public static Context getContext()
    {
        return contextWeakReference.get();
    }

    public static AppComponent getAppComponent()
    {
        return appComponent;
    }
}
