package com.cafe.domain.intractor.base;

import javax.security.auth.callback.Callback;

public interface UseCase<T , R>
{
    interface CallBack<R>
    {
        void onSuccess(R data);
        void onError  (Throwable throwable);
    }

    void execute(T  parameter , CallBack < R > callback);
}
