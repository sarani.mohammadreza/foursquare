package com.cafe.domain.intractor;


import com.cafe.domain.Repository;
import com.cafe.domain.intractor.base.UseCase;
import com.cafe.domain.model.DataMainDomain;
import com.cafe.domain.model.DataVenueInputDomain;

import java.util.List;

import io.reactivex.rxjava3.core.Single;

public class GetVenueUseCase implements UseCase<DataVenueInputDomain , Single<List<DataMainDomain>>>
{

    private final Repository repository;

    public GetVenueUseCase(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void execute(DataVenueInputDomain parameter, CallBack<Single<List<DataMainDomain>>> callback) {
        callback.onSuccess(repository.getVenue(parameter.getClientID() , parameter.getClientSecret() , parameter.getLl() , parameter.getLlAcc() , parameter.getLimit()));
    }
}
