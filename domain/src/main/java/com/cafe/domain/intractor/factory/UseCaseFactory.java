package com.cafe.domain.intractor.factory;

import com.cafe.domain.Repository;
import com.cafe.domain.intractor.GetVenueUseCase;
import com.cafe.domain.intractor.GetVenueWithNameUseCase;
import com.cafe.domain.intractor.base.UseCase;


import javax.inject.Inject;

public class UseCaseFactory
{
    private final Repository repository;

    @Inject
    public UseCaseFactory(Repository repository) {
        this.repository = repository;
    }

    public UseCase getVenueUseCase()
    {
        return new GetVenueUseCase(repository);
    }
    public UseCase GetVenueWithNameUseCase()
    {
        return new GetVenueWithNameUseCase(repository);
    }
}
