package com.cafe.domain.intractor;

import com.cafe.domain.Repository;
import com.cafe.domain.intractor.base.UseCase;
import com.cafe.domain.model.DataMainDomain;
import com.cafe.domain.model.DataVenueWithNameInputDomain;

import java.util.List;

import io.reactivex.rxjava3.core.Single;

public class GetVenueWithNameUseCase implements UseCase<DataVenueWithNameInputDomain, Single<List<DataMainDomain>>>
{

    private final Repository repository;

    public GetVenueWithNameUseCase(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void execute(DataVenueWithNameInputDomain parameter, CallBack<Single<List<DataMainDomain>>> callback) {
        callback.onSuccess(repository.getVenueWithName(parameter.getClientID() , parameter.getClientSecret() , parameter.getLl() , parameter.getLlAcc() , parameter.getIntent() , parameter.getLimit()));
    }
}