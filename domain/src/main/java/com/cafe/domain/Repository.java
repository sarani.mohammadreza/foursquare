package com.cafe.domain;

import com.cafe.domain.model.DataMainDomain;

import java.util.List;

import io.reactivex.rxjava3.core.Single;

public interface Repository
{
    Single<List<DataMainDomain>> getVenue        (String clientID , String clientSecret , String ll , double llAcc , int limit );
    Single<List<DataMainDomain>> getVenueWithName(String clientID , String clientSecret , String ll , double llAcc , String placeName ,int limit );
}
