package com.cafe.domain.model;

public class DataVenueWithNameInputDomain {
    private String clientID ,  clientSecret  , ll  , intent;
    private double llAcc ;
    private int limit;

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getLl() {
        return ll;
    }

    public void setLl(String ll) {
        this.ll = ll;
    }


    public double getLlAcc() {
        return llAcc;
    }

    public void setLlAcc(double llAcc) {
        this.llAcc = llAcc;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
