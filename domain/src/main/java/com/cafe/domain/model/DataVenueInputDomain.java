package com.cafe.domain.model;

public class DataVenueInputDomain {

    private String clientID ,  clientSecret  , ll ;
    private double llAcc ;
    private int limit;

    public DataVenueInputDomain(String clientID, String clientSecret, String ll, double llAcc, int limit) {
        this.clientID = clientID;
        this.clientSecret = clientSecret;
        this.ll = ll;
        this.llAcc = llAcc;
        this.limit = limit;
    }

    public DataVenueInputDomain() {
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getLl() {
        return ll;
    }

    public void setLl(String ll) {
        this.ll = ll;
    }


    public double getLlAcc() {
        return llAcc;
    }

    public void setLlAcc(double llAcc) {
        this.llAcc = llAcc;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
